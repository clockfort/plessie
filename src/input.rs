use std::sync::atomic::{AtomicBool, AtomicI16, AtomicU8, Ordering};
use std::sync::Arc;
use std::thread;
use std::time;

use rusty_xinput::{XInputHandle, XInputState};

#[derive(Debug)]
pub struct ControllerState {
    connected: AtomicBool,

    // XXX: Rather than mapping this as a bunch of atomic bools,
    // this is way better to just turn into two atomic u64s and bit
    // twiddle. This is just mapped this way since the xinput library
    // maps things this way and that's where I started
    pub north_button: AtomicBool,
    pub south_button: AtomicBool,
    pub east_button: AtomicBool,
    pub west_button: AtomicBool,

    pub arrow_up: AtomicBool,
    pub arrow_down: AtomicBool,
    pub arrow_right: AtomicBool,
    pub arrow_left: AtomicBool,

    pub start_button: AtomicBool,
    pub select_button: AtomicBool,

    pub left_shoulder: AtomicBool,
    pub right_shoulder: AtomicBool,

    pub left_trigger: AtomicU8,
    pub right_trigger: AtomicU8,

    pub left_thumb_button: AtomicBool,
    pub right_thumb_button: AtomicBool,
    pub left_stick_x: AtomicI16,
    pub left_stick_y: AtomicI16,
    pub right_stick_x: AtomicI16,
    pub right_stick_y: AtomicI16,
}

impl Default for ControllerState {
    fn default() -> Self {
        ControllerState {
            connected: AtomicBool::from(false),

            north_button: AtomicBool::from(false),
            south_button: AtomicBool::from(false),
            east_button: AtomicBool::from(false),
            west_button: AtomicBool::from(false),

            arrow_up: AtomicBool::from(false),
            arrow_down: AtomicBool::from(false),
            arrow_right: AtomicBool::from(false),
            arrow_left: AtomicBool::from(false),

            start_button: AtomicBool::from(false),
            select_button: AtomicBool::from(false),

            left_shoulder: AtomicBool::from(false),
            right_shoulder: AtomicBool::from(false),

            left_trigger: AtomicU8::from(0),
            right_trigger: AtomicU8::from(0),

            left_thumb_button: AtomicBool::from(false),
            right_thumb_button: AtomicBool::from(false),
            left_stick_x: AtomicI16::from(0),
            left_stick_y: AtomicI16::from(0),
            right_stick_x: AtomicI16::from(0),
            right_stick_y: AtomicI16::from(0),
        }
    }
}

impl ControllerState {
    // atomics look weird here, but they're a safe way
    // to do our bs thread updating without needing a mutable
    // reference to the controller state (note lack of `mut`)
    pub fn update_state(&self, input_state: &XInputState) {
        self.north_button
            .store(input_state.north_button(), Ordering::Relaxed);
        self.south_button
            .store(input_state.south_button(), Ordering::Relaxed);
        self.east_button
            .store(input_state.east_button(), Ordering::Relaxed);
        self.west_button
            .store(input_state.west_button(), Ordering::Relaxed);

        self.arrow_up
            .store(input_state.arrow_up(), Ordering::Relaxed);
        self.arrow_down
            .store(input_state.arrow_down(), Ordering::Relaxed);
        self.arrow_right
            .store(input_state.arrow_right(), Ordering::Relaxed);
        self.arrow_left
            .store(input_state.arrow_left(), Ordering::Relaxed);

        self.start_button
            .store(input_state.start_button(), Ordering::Relaxed);
        self.select_button
            .store(input_state.select_button(), Ordering::Relaxed);

        self.left_shoulder
            .store(input_state.left_shoulder(), Ordering::Relaxed);
        self.right_shoulder
            .store(input_state.right_shoulder(), Ordering::Relaxed);

        self.left_trigger
            .store(input_state.left_trigger(), Ordering::Relaxed);
        self.right_trigger
            .store(input_state.right_trigger(), Ordering::Relaxed);

        self.left_thumb_button
            .store(input_state.left_thumb_button(), Ordering::Relaxed);
        self.right_thumb_button
            .store(input_state.right_thumb_button(), Ordering::Relaxed);
        let (x, y) = input_state.left_stick_raw();
        self.left_stick_x.store(x, Ordering::Relaxed);
        self.left_stick_y.store(y, Ordering::Relaxed);
        let (x, y) = input_state.right_stick_raw();
        self.right_stick_x.store(x, Ordering::Relaxed);
        self.right_stick_y.store(y, Ordering::Relaxed);
    }
}

pub struct Controller {
    // don't make these pub
    pub id: u32,
    pub state: Arc<ControllerState>,
    handle: Arc<XInputHandle>,
}

impl Controller {
    pub fn new(id: u32, handle: Arc<XInputHandle>) -> Self {
        Controller {
            id,
            state: Arc::new(ControllerState::default()),
            handle,
        }
    }

    pub fn run(&self) {
        loop {
            if self.handle.get_state(self.id).is_ok() {
                println!("Controller {} connected", self.id);
                self.state.connected.store(true, Ordering::Relaxed);
                loop {
                    match self.handle.get_state(self.id) {
                        Ok(state) => {
                            self.state.update_state(&state);
                            //println!("controller {} state: {:?}", self.id, self.state);
                        }
                        Err(_) => {
                            println!("Controller {} disconnected", self.id);
                            self.state.connected.store(true, Ordering::Relaxed);
                            break;
                        }
                    }

                    // we can poll more often, but that fucks CPU
                    thread::sleep(time::Duration::from_micros(1))
                }
            }

            // poll for connections
            thread::sleep(time::Duration::from_secs(10));
        }
    }

    pub fn connected(&self) -> bool {
        self.state.connected.load(Ordering::Relaxed)
    }
}

use std::sync::Arc;
use std::sync::atomic::Ordering;
use std::thread;
use std::time;

use rusty_xinput::XInputHandle;

mod input;
mod output;

const MAX_CONTROLLERS: u32 = 4;

fn init_plessie() -> output::PlessieController {
    let mut p = output::PlessieController::new();
    p.connect();
    p
}

// this is kept separate because I'd like to make these passable
fn calculate_state(controllers: &[Arc<input::Controller>]) -> output::PlessieControllerState {
    let mut staging = output::PlessieControllerState::default();
    staging.north_button = true;
    staging.south_button = true;
    staging.east_button = true;
    staging.west_button = true;

    staging.arrow_up = true;
    staging.arrow_down = true;
    staging.arrow_right = true;
    staging.arrow_left = true;

    staging.start_button = true;
    staging.select_button = true;

    staging.left_shoulder = true;
    staging.right_shoulder = true;

    staging.left_thumb_button = true;
    staging.right_thumb_button = true;

    let mut left_stick_x_accum: i64 = 0;
    let mut left_stick_y_accum: i64 = 0;
    let mut right_stick_x_accum: i64 = 0;
    let mut right_stick_y_accum: i64 = 0;

    let mut left_trigger_accum: u64 = 0;
    let mut right_trigger_accum: u64 = 0;

    let mut connected = 0;

    for controller in controllers {
        if ! controller.connected() {
            continue;
        }

        connected += 1;

        staging.north_button &= controller.state.north_button.load(Ordering::Relaxed);
        staging.south_button &= controller.state.south_button.load(Ordering::Relaxed);
        staging.east_button &= controller.state.east_button.load(Ordering::Relaxed);
        staging.west_button &= controller.state.west_button.load(Ordering::Relaxed);

        staging.arrow_up &= controller.state.arrow_up.load(Ordering::Relaxed);
        staging.arrow_down &= controller.state.arrow_down.load(Ordering::Relaxed);
        staging.arrow_right &= controller.state.arrow_right.load(Ordering::Relaxed);
        staging.arrow_left &= controller.state.arrow_left.load(Ordering::Relaxed);

        staging.start_button &= controller.state.start_button.load(Ordering::Relaxed);
        staging.select_button &= controller.state.select_button.load(Ordering::Relaxed);

        staging.left_shoulder &= controller.state.left_shoulder.load(Ordering::Relaxed);
        staging.right_shoulder &= controller.state.right_shoulder.load(Ordering::Relaxed);

        staging.left_thumb_button &= controller.state.left_thumb_button.load(Ordering::Relaxed);
        staging.right_thumb_button &= controller.state.right_thumb_button.load(Ordering::Relaxed);

        left_stick_x_accum += controller.state.left_stick_x.load(Ordering::Relaxed) as i64;
        left_stick_y_accum += controller.state.left_stick_y.load(Ordering::Relaxed) as i64;

        right_stick_x_accum += controller.state.right_stick_x.load(Ordering::Relaxed) as i64;
        right_stick_y_accum += controller.state.right_stick_y.load(Ordering::Relaxed) as i64;

        left_trigger_accum += controller.state.left_trigger.load(Ordering::Relaxed) as u64;
        right_trigger_accum += controller.state.right_trigger.load(Ordering::Relaxed) as u64;
    }

    if connected != 0 {
        staging.left_stick_x = (left_stick_x_accum / connected as i64) as i16;
        staging.left_stick_y = (left_stick_y_accum / connected as i64) as i16;
        staging.right_stick_x = (right_stick_x_accum / connected as i64) as i16;
        staging.right_stick_y = (right_stick_y_accum / connected as i64) as i16;
        staging.left_trigger = (left_trigger_accum / connected as u64) as u8;
        staging.right_trigger = (right_trigger_accum / connected as u64) as u8;
    }

    //println!("staging: {:?}", staging);
    staging
}

fn main() {
    let handle = Arc::new(XInputHandle::load_default().unwrap());
    let mut controllers = Vec::new();
    let mut plessie = init_plessie();

    for id in 0..MAX_CONTROLLERS {
        // poison plessie controller number
        if id == plessie.controller_number.unwrap() {
            continue;
        }

        let thandle = handle.clone();
        let controller = Arc::new(input::Controller::new(id, thandle));
        let c = controller.clone();

        thread::spawn(move || c.run());
        controllers.push(controller);
    }

    loop {
        let state = calculate_state(&controllers);
        plessie.update_state(state);

        thread::sleep(time::Duration::from_micros(1));
    }
}

use vigem::{self, Vigem, XButton, XUSBReport};

#[derive(Debug)]
pub struct PlessieControllerState {
    pub north_button: bool,
    pub south_button: bool,
    pub east_button: bool,
    pub west_button: bool,

    pub arrow_up: bool,
    pub arrow_down: bool,
    pub arrow_right: bool,
    pub arrow_left: bool,

    pub start_button: bool,
    pub select_button: bool,

    pub left_shoulder: bool,
    pub right_shoulder: bool,

    pub left_trigger: u8,
    pub right_trigger: u8,

    pub left_thumb_button: bool,
    pub right_thumb_button: bool,
    pub left_stick_x: i16,
    pub left_stick_y: i16,
    pub right_stick_x: i16,
    pub right_stick_y: i16,
}

impl Default for PlessieControllerState {
    fn default() -> Self {
        PlessieControllerState {
            north_button: false,
            south_button: false,
            east_button: false,
            west_button: false,

            arrow_up: false,
            arrow_down: false,
            arrow_right: false,
            arrow_left: false,

            start_button: false,
            select_button: false,

            left_shoulder: false,
            right_shoulder: false,

            left_trigger: 0,
            right_trigger: 0,

            left_thumb_button: false,
            right_thumb_button: false,
            left_stick_x: 0,
            left_stick_y: 0,
            right_stick_x: 0,
            right_stick_y: 0,
        }
    }
}

pub struct PlessieController {
    handle: Vigem,
    target: vigem::Target,
    state: PlessieControllerState,
    pub controller_number: Option<u32>,
}

impl PlessieController {
    pub fn new() -> Self {
        PlessieController {
            handle: Vigem::new(),
            target: vigem::Target::new(vigem::TargetType::Xbox360),
            state: PlessieControllerState::default(),
            controller_number: None,
        }
    }

    pub fn connect(&mut self) {
        self.handle.connect().expect("Could not connect");
        self.handle.target_add(&mut self.target).expect("Can't add target");
        //self.controller_number = Some(self.target.index());
        self.controller_number = Some(self.handle.xbox_get_user_index(&self.target));
    }

    pub fn update_state(&mut self, new_state: PlessieControllerState) {
        self.state = new_state;

        // these naming incongruencies suck
        let mut but = XButton::empty();
        but.set(XButton::Y, self.state.north_button);
        but.set(XButton::A, self.state.south_button);
        but.set(XButton::B, self.state.east_button);
        but.set(XButton::X, self.state.west_button);

        but.set(XButton::DpadUp, self.state.arrow_up);
        but.set(XButton::DpadDown, self.state.arrow_down);
        but.set(XButton::DpadRight, self.state.arrow_right);
        but.set(XButton::DpadLeft, self.state.arrow_left);

        but.set(XButton::Start, self.state.start_button);
        but.set(XButton::Back, self.state.select_button);

        but.set(XButton::LeftShoulder, self.state.left_shoulder);
        but.set(XButton::RightShoulder, self.state.right_shoulder);

        but.set(XButton::LeftThumb, self.state.left_thumb_button);
        but.set(XButton::RightThumb, self.state.right_thumb_button);

        let report = XUSBReport{
            w_buttons: but,
            b_left_trigger: self.state.left_trigger,
            b_right_trigger: self.state.right_trigger,
            s_thumb_lx: self.state.left_stick_x,
            s_thumb_ly: self.state.left_stick_y,
            s_thumb_rx: self.state.right_stick_x,
            s_thumb_ry: self.state.right_stick_y,
        };

        self.handle.update(&self.target, &report).expect("could not update controller");
    }
}